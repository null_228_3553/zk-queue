package com.gp.test;

/**
 * 节点实体
 *
 * @author
 * @create 2017-08-12
 */
public class Node<String, T> {
    /**
     * 节点路径名称
     */
    private String nodeName;
    /**
     * 节点数据值
     */
    private T nodeValue;

    public Node(String nodeName, T nodeValue) {
        this.nodeName = nodeName;
        this.nodeValue = nodeValue;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public T getNodeValue() {
        return nodeValue;
    }

    public void setNodeValue(T nodeValue) {
        this.nodeValue = nodeValue;
    }
}
