package com.gp.test;

import java.util.List;

/**
 * 分布式队列
 *
 * @author
 * @create 2017-08-12
 */
public interface DistributedQueue<T> {

    /**
     * 向队列中生产数据
     *
     * @param data 存入队列的数据
     */
    void produce(T data);

    /**
     * 向队列中批量生产数据
     *
     * @param datas 存入队列的数据
     */
    void produce(List<T> datas);

    /**
     * 从队列中消费数据
     *
     * @return 队列中的一个节点（包含节点路径名称和节点数据）
     */
    Node<String, T> counsume();
}
