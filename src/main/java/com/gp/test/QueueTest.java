package com.gp.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 分布式队列测试
 *
 * @author
 * @create 2017-08-12
 */
public class QueueTest {
    public static final String queueNode = "/queue";

    public static void main(String[] args) {
        FIFODistributedQueue<String> queue = new FIFODistributedQueue<String>(queueNode);

        new Thread(new ConsumerThread(queue)).start();
        new Thread(new ConsumerThread(queue)).start();
        new Thread(new ProducerThread(queue)).start();
    }
}

class ProducerThread implements Runnable {
    private DistributedQueue<String> queue;

    public ProducerThread(DistributedQueue<String> queue) {
        this.queue = queue;
    }

    public void run() {
//        for (int i = 0; i < 50; i++) {
//            queue.produce("queue_" + i);
//            try {
//                TimeUnit.MILLISECONDS.sleep(2);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        List<String> datas = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            datas.add("queue_" + i);
        }
        queue.produce(datas);

    }
}

class ConsumerThread implements Runnable {
    private DistributedQueue<String> queue;

    public ConsumerThread(DistributedQueue<String> queue) {
        this.queue = queue;
    }

    public void run() {
        while (true) {
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Node<String, String> node = this.queue.counsume();
            if (node == null) {
                continue;
            }
            System.out.println(Thread.currentThread().getName() + " consume node " + node.getNodeName() + ":" + node.getNodeValue());
        }
    }
}
