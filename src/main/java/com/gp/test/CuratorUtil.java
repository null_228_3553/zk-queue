package com.gp.test;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * curator工具类
 *
 * @author
 * @create 2017-08-12
 */
public class CuratorUtil {
    private static String connectString = "192.168.19.128:2181,192.168.19.129:2181," +
            "192.168.19.130:2181";
    private static int sessionTimeoutMs = 5000;
    private static int connectionTimeoutMs = 5000;
    private static int baseSleepTimeMs = 1000;
    private static int maxRetries = 3;

    public static CuratorFramework getInstance() {
        CuratorFramework curatorFramework = CuratorFrameworkFactory.
                newClient(connectString, sessionTimeoutMs, connectionTimeoutMs,
                        new ExponentialBackoffRetry(baseSleepTimeMs, maxRetries));
        curatorFramework.start();
        return curatorFramework;
    }
}
